from setuptools import setup
import glob

setup(name='atcf_mirror',
      description='Downloader of ATCF Best, Fix and Forecast Tracks from NHC and JTWC',
      url='https://gitlab.ifremer.fr/cyclobs/atcf_mirror',
      author = "Virgile Silvant",
      author_email = "Virgile.Silvant@ifremer.fr",
      license='GPL',
      scripts=glob.glob('bin/**'),
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      packages=['atcf_mirror'],
      install_requires=[],
      zip_safe=False
      )
