#!/bin/bash

# Notes :
# A conf file must be created on

# Example
# lftp ftp://identifiant:mot_de_passe@site_de_connexion -e "mirror -e -x dossier_ignoré -x dossier_ignoré /emplacement_distant /emplacement_local ; quit"

# FTP URL
protocol="ftp" # "sftp" for ssl
user="anonymous"
pass="pass"
host="ftp.nhc.noaa.gov"
ftpurl="$protocol://$user:$pass@$host"

# LFTP parameters
lftp_sets="set cmd:fail-exit yes;"
remote_dir="/atcf/"
# TODO : change the local path
local_dir="/home3/homedir7/perso/vsilvant/Documents/test/nhc_ftp/"

# === Download Current === #
# Mirror : Forecast
remotecd=$remote_dir"fst/"
localcd=$local_dir"fst/"
lftp -e "$lftp_sets open '$ftpurl'; lcd $localcd; cd $remotecd;
mirror --continue --only-newer --delete --verbose=3 --no-perms --parallel=8; quit;"

# Mirror : Best-Track
remotecd=$remote_dir"btk/"
localcd=$local_dir"btk/"
lftp -e "$lftp_sets open '$ftpurl'; lcd $localcd; cd $remotecd;
mirror --continue --only-newer --delete --verbose=3 --no-perms --parallel=8; quit;"

# Mirror : Fix
remotecd=$remote_dir"fix/"
localcd=$local_dir"fix/"
lftp -e "$lftp_sets open '$ftpurl'; lcd $localcd; cd $remotecd;
mirror --continue --only-newer --delete --verbose=3 --no-perms --parallel=8; quit;"

# === Download Archive === #
# Mirror the years of archives we want
remotecd=$remote_dir"archives/"
localcd=$local_dir"archives/"
includes="2020/"
lftp -e "$lftp_sets open '$ftpurl'; lcd $localcd; cd $remotecd;
mirror --exclude '.*' --exclude '.*/' --continue --only-newer --delete --verbose=3 --no-perms --parallel=8 --include $includes; quit;"

# Then, sort them between forecast and best-track