#!/usr/bin/env python

import logging
import os

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)

if __name__ == "__main__":
    a = 0