# Official ATCF mirroring from remote sources

## Installation

The installation is done with the command :
> pip install -e .

in a Conda environment.

## Create a mirror of all official ATCF Tracks

Create a config file named `atcf_mirror/etc/atcf_mirror.conf`, and fill it with working user and password :
> jtwc_id="xxxxx"
>
> jtwc_password="xxxxx"

*If needed, call the Cyclobs team at Ifremer to get an access*

Then, execute the command :
> atcf_mirror.sh

## Explanation

__The script `bin/atcf_mirror.sh` will launch the two scripts :__
- `bin/mirror_nhc.sh` who retrieve via LFTP the NHC data from ftp://ftp.nhc.noaa.gov/atcf/
- `bin/mirror_jtwc.py` who scrap via a web request the JTWC data from https://pzal.metoc.navy.mil/php/rds/m2m/index.php/nrlhhc/

*This way the project propose a mirror of all official ATCF tracks that is maintained up to date.*

## Work in progress

- `bin/mirror_nhc.sh` isn't complete, the archives and current storms are still separated.
- `bin/mirror_jtwc.py` is still to be completed